# iCAR
An API wrapper for Stif iCAR API

# Dependencies
Ruby >= 2.1.0
Libmagic

# Install
Build
```ruby
git clone git@bitbucket.org:enroute-mobi/icar-api.git
cd icar-api
make
```

Add to your project
```ruby
gem 'icar'
bundle
```

Usage
```ruby
client  = ICar::API.new
results = client.process 'getOP'
```

You can set timeout and override api base url globally in your config/initializers/icar.rb

```ruby
ICar::API.base_url = "https://pprod-icar.iledefrance-mobilites.fr/ws/rest/v2/getData"
ICar::API.timeout  = 50
```

# Tests
```ruby
cd icar-api
make tests
```
