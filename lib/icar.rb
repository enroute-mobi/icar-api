require "open-uri"
require "nokogiri"
require "zip"
require "rgeo"
require "rgeo/proj4"
require "filemagic"

require "icar/api"
require "icar/icar_error"
require "icar/stop_place"
require "icar/stop_place_entrance"
require "icar/quay"
require "icar/organisational_unit"
require "icar/lamber"

module ICar
end
