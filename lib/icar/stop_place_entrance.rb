module ICar
  class StopPlaceEntranceNodeHandler < Nokogiri::XML::SAX::Document
    def start_document
      @stop_place           = {}
      @text_stack           = []
    end

    def end_document
      @stop_place_entrance['type'] = 'StopPlace'
      @stop_place_entrance['gml:pos'] = LamberWilson.to_longlat(@stop_place_entrance['gml:pos'])
      API.stop_place_entrances << @stop_place_entrance
    end

    def start_element(name, attrs = [])
      @stop_place_entrance           = Hash[attrs]        if name == 'StopPlaceEntrance'
    end

    def characters(string)
      string = string.gsub("\n", '').strip
      @text_stack << string unless string.empty?
    end

    def end_element(name)
      string = @text_stack.join
      @text_stack = []
      return if string.empty?

      @stop_place_entrance[name] = @stop_place_entrance[name].to_s + string
    end
  end
end
