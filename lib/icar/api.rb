module ICar
  class API
    DEFAULT_TIMEOUT  = 30
    DEFAULT_FORMAT   = 'xml'
    DEFAULT_BASE_URL = "https://pprod-icar.iledefrance-mobilites.fr/ws/rest/v2/getData"
    @quays       = []
    @stop_places = []
    @organisational_units = []
    @stop_place_entrances = []

    attr_accessor :timeout, :format, :base_url, :token

    def initialize(timeout: nil, format: nil, base_url: nil, token: nil)
      @timeout  = timeout || self.class.timeout || DEFAULT_TIMEOUT
      @format   = format || self.class.format || DEFAULT_FORMAT
      @base_url = base_url || self.class.base_url || DEFAULT_BASE_URL
      @token = token || self.class.token
    end

    def build_url(params = {})
      query = params.map{|k, v| [k,v].join('=') }.to_a.join('&')
      "#{self.base_url}?#{query}"
    end

    def api_request(params = {})
      raise ICar::ICarError, "No method specified" if params[:method].nil?
      url = build_url(params)
      begin
        request_options = {
          :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE,
          :read_timeout => @timeout,
        }
        request_options["Authorization"] = "Bearer #{token}" if token

        result = URI.open(url, request_options)
        return result unless result.is_a? StringIO

        tmpfile = Tempfile.new("file")
        tmpfile.binmode
        tmpfile.write(result.read)
        tmpfile.close
        tmpfile
      rescue Exception => e
        raise ICar::ICarError, "#{e.message} for request : #{url}."
      end
    end

    def parse_response file
      begin
        Nokogiri::XML::Reader(file)
      rescue Exception => e
        raise ICar::ICarError, e.message
      end
    end

    def reset_processed
      self.class.stop_places = []
      self.class.quays = []
      self.class.organisational_units = []
      self.class.stop_place_entrances = []
    end

    def process method
      self.process_with_params(method: method)
    end

    def process_with_params params
      apifile = self.api_request params
      file = File.open(apifile.path, "r")
      reader =  self.parse_response(file)

      reader.each do |node|
        next unless node.node_type == Nokogiri::XML::Reader::TYPE_ELEMENT
        next unless ['StopPlace', 'Quay', 'OrganisationalUnit', 'StopPlaceEntrance'].include?(node.name)

        xml = node.outer_xml
        if node.name == 'StopPlace'
          Nokogiri::XML::SAX::Parser.new(StopPlaceNodeHandler.new).parse(xml)
          self.class.stop_places.last[:xml] = xml
        end

        if node.name == 'Quay'
          Nokogiri::XML::SAX::Parser.new(QuayNodeHandler.new).parse(xml)
          self.class.quays.last[:xml] = xml
        end

        if node.name == 'OrganisationalUnit'
          Nokogiri::XML::SAX::Parser.new(OrganisationalUnitNodeHandler.new).parse(xml)
          self.class.organisational_units.last[:xml] = xml
        end

        if node.name == 'StopPlaceEntrance'
          Nokogiri::XML::SAX::Parser.new(StopPlaceEntranceNodeHandler.new).parse(xml)
          self.class.stop_place_entrances.last[:xml] = xml
        end
      end
      results = {
        :StopPlace => self.class.stop_places,
        :Quay      => self.class.quays,
        :OrganisationalUnit  => self.class.organisational_units,
        :StopPlaceEntrance  => self.class.stop_place_entrances
      }
      self.reset_processed
      results
    ensure
      file.close unless file.nil?
    end

    class << self
      attr_accessor :timeout, :format, :base_url, :token, :quays, :stop_places, :organisational_units, :stop_place_entrances
    end
  end
end
