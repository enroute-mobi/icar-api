module ICar
  class LamberWilson
    include Singleton

    def initialize
      @lamber = RGeo::CoordSys::Proj4.create("EPSG:2154")
      @wgs84  = RGeo::CoordSys::Proj4.create("EPSG:4326")
    end

    def self.to_longlat(coords)
      instance.to_longlat coords
    end

    def to_longlat(coords)
      return nil unless coords

      coords = coords.split(' ') if coords.is_a?(String)
      coords = coords.map(&:to_f)
      
      x, y = coords

      longitude, latitude = RGeo::CoordSys::Proj4.transform_coords(lamber, wgs84, x, y, nil)
      { lng: longitude, lat: latitude }
    end

    private 

    attr_reader :lamber, :wgs84
  end
end
